/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package jfractionlab.exerciseDialogs;

import java.awt.Color;
import java.awt.Container;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import jfractionlab.JFractionLab;
import jfractionlab.MyJTextField;
import jfractionlab.displays.PointDisplay;


public abstract class ExerciseDialog extends JDialog {
	
	static final long serialVersionUID = JFractionLab.serialVersionUID;
	
	//GUI	
	JMenuBar jmb = new JMenuBar();
	JMenu jmOptions = new JMenu("");
	ButtonGroup btngrp_typeoftask = new ButtonGroup();
	JRadioButtonMenuItem rb_random = new JRadioButtonMenuItem();
	JRadioButtonMenuItem rb_custom = new JRadioButtonMenuItem();
	
	ButtonGroup btngrp_reducing = new ButtonGroup();
	public JRadioButtonMenuItem rb_reducing = new JRadioButtonMenuItem();
	JRadioButtonMenuItem rb_no_reducing = new JRadioButtonMenuItem();
	
	ButtonGroup btngrp_visibility = new ButtonGroup();
	JRadioButtonMenuItem rb_visible = new JRadioButtonMenuItem();
	JRadioButtonMenuItem rb_invisible = new JRadioButtonMenuItem();
	
	ButtonGroup btngrp_BigSmallNb = new ButtonGroup();
	JRadioButtonMenuItem rb_smallNb = new JRadioButtonMenuItem();
	JRadioButtonMenuItem rb_bigNb =  new JRadioButtonMenuItem();
	
	JMenu jmWorkSheet = new JMenu("");
	JMenuItem jmiCreateWorkSheet = new JMenuItem("");
	
	JMenu jmHelp = new JMenu("");
	JMenuItem jmiHelp = new JMenuItem("");
	
	Container content;
	public JLabel lb_info = new JLabel("", JLabel.CENTER);
	//JLabel lb_info = new JLabel("", javax.swing.SwingConstants.CENTER);
	PointDisplay pdsp = new PointDisplay();
	JButton btn_continue;
	JButton btn_end;
	int points = 0;
	ArrayList<MyJTextField> al_textfields = new ArrayList<MyJTextField>();
	JFractionLab owner;
	public boolean bl_with_reducing = false;
	boolean bl_randomProblem = true;
	boolean smallNumbers = true;
	public boolean bl_wannaReduceQuestion_AnswerIsYes = false;
	public boolean bl_WannaFindBestCommonDenominator_AnswerIsYes = false;
	
	public ExerciseDialog(){}
	public ExerciseDialog(int lx, int ly, int sx, int sy){
		super();
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		lb_info.setFont(JFractionLab.infofont);
		btn_continue = new JButton(lang.Messages.getString("continue"));
		btn_end =  new JButton(lang.Messages.getString("end"));
		rb_random.setText(lang.Messages.getString("random_exercises"));
		
		content = getContentPane();
		content.setBackground(Color.white);
		
		jmOptions.setText(lang.Messages.getString("options"));
		
		rb_random.setText(lang.Messages.getString("random_exercises"));
		rb_random.setSelected(true);
		btngrp_typeoftask.add(rb_random);
		rb_custom.setText(lang.Messages.getString("custom_problems"));
		btngrp_typeoftask.add(rb_custom);
		
		rb_no_reducing.setText(lang.Messages.getString("without_reducing"));
		rb_no_reducing.setSelected(true);
		btngrp_reducing.add(rb_no_reducing);
		rb_reducing.setText(lang.Messages.getString("with_reducing"));
		btngrp_reducing.add(rb_reducing);
		
		rb_visible.setText(lang.Messages.getString("show_pies"));
		rb_visible.setSelected(true);
		btngrp_visibility.add(rb_visible);
		rb_invisible.setText(lang.Messages.getString("hide_pies"));
		btngrp_visibility.add(rb_invisible);
		
		rb_smallNb.setText(lang.Messages.getString("small_numbers"));
		rb_smallNb.setSelected(true);
		btngrp_BigSmallNb.add(rb_smallNb);
		rb_bigNb.setText(lang.Messages.getString("big_numbers"));
		btngrp_BigSmallNb.add(rb_bigNb);
		
		jmWorkSheet.setText(lang.Messages.getString("worksheets"));
		jmiCreateWorkSheet.setText(lang.Messages.getString("generate_worksheets"));
		jmWorkSheet.add(jmiCreateWorkSheet);
		
		jmHelp.setText(lang.Messages.getString("jmi_help"));
		jmiHelp.setText(lang.Messages.getString("jmi_help"));
		jmHelp.add(jmiHelp);
		
		
		setSize(sx,sy);
		setLocation(lx,ly);
		setResizable(true);
		setVisible(true);
		
	}//Konstruktor
	
	public void addTextField(MyJTextField tf){
		al_textfields.add(tf);
	}
	
	public void clearTextFields(){
		for(int i = 0; i < al_textfields.size(); i++){
			al_textfields.get(i).setDumb();
			//sop("clear tf = "+i);
		}
	}
	
	public Integer readInputInt(MyJTextField jtf){
		Integer ret;
		try{
			ret = Integer.parseInt(jtf.getText());
			if (ret < 1){
				ret = null;
				jtf.setText("");
				lb_info.setText(lang.Messages.getString("no_null"));
			}
		}catch(NumberFormatException nfe){
			nfe.printStackTrace();
			ret = null;
			jtf.setText("");
			lb_info.setText(lang.Messages.getString("nb_is_not_correct"));
		}
		return ret;
	}//readInputNumber

	public Double readInputDouble(MyJTextField jtf){
		Double ret;
		try{
			String str= jtf.getText();
			if(str.contains(",")){
				str = str.replace(",",".");
			}
			ret = Double.parseDouble(str);
		}catch(NumberFormatException nfe){
			nfe.printStackTrace();
			ret = null;
			jtf.setText("");
			lb_info.setText(lang.Messages.getString("nb_is_not_correct"));
		}
		return ret;
	}//readInputDouble
	
	abstract protected void nextProblem();
	abstract protected void makeProblem();
	
	public void close_it(){
		setVisible(false);
	        dispose();
	}//close_it()

	public void sop(String str){System.out.println(str);}
}
