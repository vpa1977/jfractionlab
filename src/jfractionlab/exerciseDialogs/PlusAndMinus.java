/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/


package jfractionlab.exerciseDialogs;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JLabel;

import jfractionlab.FractionMaker;
import jfractionlab.HelpStarter;
import jfractionlab.JFractionLab;
import jfractionlab.MyJTextField;
import jfractionlab.displays.FractionAsCircle;
import jfractionlab.displays.FractionLine;
import jfractionlab.exerciseGenerator.ExerciseGenerator;
import jfractionlab.jflDialogs.UsabilityDialog;
import jfractionlab.jflDialogs.WannaFindBestCommonDenominator;
import jfractionlab.jflDialogs.WannaReduceQuestion;
import jfractionlab.jflDialogs.WorkSheetDialog;



public class PlusAndMinus extends ExerciseDialog implements ActionListener, KeyListener{
	static final long serialVersionUID = JFractionLab.serialVersionUID;	
	//GUI
	private MyJTextField tf_numerator1 = new MyJTextField(3);
	private MyJTextField tf_numerator2 = new MyJTextField(3);
	private MyJTextField tf_calculation_numerator1 = new MyJTextField(3);
	private MyJTextField tf_calculation_numerator2 = new MyJTextField(3);
	private MyJTextField tf_provisionalresult_numerator = new MyJTextField(3);
	private MyJTextField tf_endresult_numerator = new MyJTextField(3);

	private MyJTextField tf_denominator1 = new MyJTextField(3);
	private MyJTextField tf_denominator2 = new MyJTextField(3);
	private MyJTextField tf_calculation_denominator1 = new MyJTextField(3);
	private MyJTextField tf_calculation_denominator2 = new MyJTextField(3);
	private MyJTextField tf_provisionalresult_denominator = new MyJTextField(3);
	private MyJTextField tf_endresult_denominator = new MyJTextField(3);
	private JLabel lb_number_one = new JLabel("", JLabel.CENTER);
	private JLabel lb_equalsign_endresult = new JLabel("=", JLabel.CENTER);
	private FractionAsCircle pizza_1 = new FractionAsCircle();
	private FractionAsCircle pizza_2 = new FractionAsCircle();
	private FractionAsCircle calculation_pizza_1 = new FractionAsCircle();
	private FractionAsCircle calculation_pizza_2 = new FractionAsCircle();
	private FractionAsCircle provisionalresult_pizza_1 = new FractionAsCircle();
	private FractionAsCircle provisionalresult_pizza_2 = new FractionAsCircle();
	private FractionAsCircle endresult_pizza_1 = new FractionAsCircle();
	private FractionAsCircle endresult_pizza_2 = new FractionAsCircle();	

	//Zahlen
	private FractionMaker fraction;
	private int numerator1;
	private int denominator1;
	private int numerator2;
	private int denominator2;
	private int common_denominator;
	private int calculation_numerator1;
	private int calculation_numerator2;
	private int provisionalresult_numerator;
	private int endresult_numerator;
	private String operator;
	

	
	/**
	 * 
	 * @param owner
	 * @param operator
	 * @param lx
	 * @param ly
	 * @param sx
	 * @param sy
	 * @throws HeadlessException
	 */
	public PlusAndMinus(JFractionLab owner, String operator, int lx, int ly, int sx, int sy) throws HeadlessException {
		super(lx, ly, sx, sy);
		this.owner = owner;
		this.operator = operator;
		if(operator.equals("+")){
			setTitle(lang.Messages.getString("add_the_fracs"));
		}else if(operator.equals("-")){
			setTitle(lang.Messages.getString("sub_the_fracs"));
		}else{
			//ERROR
		}
		//Menu
		rb_random.addActionListener(this);
		jmOptions.add(rb_random);
		rb_custom.addActionListener(this);
		jmOptions.add(rb_custom);
		jmOptions.addSeparator();
		rb_no_reducing.addActionListener(this);
		jmOptions.add(rb_no_reducing);
		rb_reducing.addActionListener(this);
		jmOptions.add(rb_reducing);
		jmb.add(jmOptions);
		
		jmiCreateWorkSheet.addActionListener(this);
		jmWorkSheet.add(jmiCreateWorkSheet);
		jmb.add(jmWorkSheet);
		
		jmiHelp.addActionListener(this);
		jmb.add(jmHelp);
		setJMenuBar(jmb);
		
		double sizes[][] = {{
			// Spalten
			0.02, 0.10, 0.02,
			0.02, 0.10, 0.02,
			0.02, 0.10, 0.02,
			0.02, 0.10, 0.02,
			0.02, 0.10, 0.02,
			0.02, 0.05, 0.05, 0.02,
			TableLayout.FILL, 0.02
		},{
			//Zeilen
			TableLayout.FILL,
			12,
			TableLayout.FILL,
			0.40,
			0.40,
			30,
		}};

        	content.setLayout(new TableLayout(sizes));
		
		//--Zaehler
        	addTextField(tf_numerator1);
			tf_numerator1.addKeyListener(this);
			tf_numerator1.setEditable(false);
		content.add(tf_numerator1, "1,0,c,b");
			addTextField(tf_numerator2);
			tf_numerator2.addKeyListener(this);
			tf_numerator2.setEditable(false);
        content.add(tf_numerator2, "4,0,c,b");
        	addTextField(tf_calculation_numerator1);
			tf_calculation_numerator1.addKeyListener(this);
			tf_calculation_numerator1.setEditable(false);
		content.add(tf_calculation_numerator1, "7,0,c,b");
			addTextField(tf_calculation_numerator2);
			tf_calculation_numerator2.addKeyListener(this);
			tf_calculation_numerator2.setEditable(false);
		content.add(tf_calculation_numerator2, "10,0,c,b");
			addTextField(tf_provisionalresult_numerator);
			tf_provisionalresult_numerator.addKeyListener(this);
			tf_provisionalresult_numerator.setEditable(false);
		content.add(tf_provisionalresult_numerator, "13,0,c,b");
			addTextField(tf_endresult_numerator);
			tf_endresult_numerator.addKeyListener(this);
			tf_endresult_numerator.setEditable(false);
		content.add(tf_endresult_numerator, "17,0,c,b");
		//--Nenner
    		addTextField(tf_denominator1);
    		tf_denominator1.addKeyListener(this);
    		tf_denominator1.setEditable(false);
    	content.add(tf_denominator1, "1,2,c,t");
    		addTextField(tf_denominator2);
    		tf_denominator2.addKeyListener(this);
    		tf_denominator2.setEditable(false);
		content.add(tf_denominator2, "4,2,c,t");
			addTextField(tf_calculation_denominator1);
			tf_calculation_denominator1.addKeyListener(this);
		content.add(tf_calculation_denominator1, "7,2,c,t");
			addTextField(tf_calculation_denominator2);
		content.add(tf_calculation_denominator2, "10,2,c,t");
		content.add(tf_provisionalresult_denominator, "13,2,c,t");
			addTextField(tf_endresult_denominator);
			tf_endresult_denominator.addKeyListener(this);
			tf_endresult_denominator.setEditable(false);
		content.add(tf_endresult_denominator, "17,2,c,t");
		content.add(lb_number_one, "16,0,16,2,c,c");
		//--Zeichen
			JLabel lb_aufgabenPlus = new JLabel(operator, JLabel.CENTER);
		content.add(lb_aufgabenPlus, "2,1,3,1");
       			JLabel lb_aufgabenPizza = new JLabel(operator, JLabel.CENTER);
		content.add(lb_aufgabenPizza, "2,3,3,3");
		JLabel lb_calculationPlus = new JLabel(operator, JLabel.CENTER);
		content.add(lb_calculationPlus, "8,1,9,1,c,b");
		
			JLabel lb_calculationPizza = new JLabel(operator, JLabel.CENTER);
		content.add(lb_calculationPizza, "8,3,9,3");
			JLabel lb_aufgabenGleich = new JLabel("=", JLabel.CENTER);
		content.add(lb_aufgabenGleich, "5,1,6,1");
			JLabel lb_aufgabenPizzaGleich = new JLabel("=", JLabel.CENTER);
		content.add(lb_aufgabenPizzaGleich, "5,3,6,3");
			JLabel lb_calculationGleich = new JLabel("=", JLabel.CENTER);
		content.add(lb_calculationGleich, "11,1,12,1");
			JLabel lb_calculationPizzaGleich = new JLabel("=", JLabel.CENTER);
		content.add(lb_calculationPizzaGleich, "11,3,12,3");
			JLabel lb_rundungsGleich = new JLabel("=", JLabel.CENTER);
		content.add(lb_rundungsGleich, "14,1,15,1");
		content.add(lb_equalsign_endresult, "14,3,15,3");
		
			FractionLine bs_fraction1 = new FractionLine();
		content.add(bs_fraction1, "1,1");
			FractionLine bs_fraction2 = new FractionLine();
		content.add(bs_fraction2, "4,1");
			FractionLine bs_rechnung1 = new FractionLine();
		content.add(bs_rechnung1, "7,1");
			FractionLine bs_rechnung2 = new FractionLine();
		content.add(bs_rechnung2, "10,1");
			FractionLine bs_ergebnis = new FractionLine();
		content.add(bs_ergebnis, "13,1");
			FractionLine bs_gerundetesErgebnis = new FractionLine();
        	content.add(bs_gerundetesErgebnis, "17,1");

		//--Pizzen
		content.add(pizza_1, "1,3,");
		content.add(pizza_2, "4,3");
		content.add(calculation_pizza_1, "7,3");
		content.add(calculation_pizza_2, "10,3");
		content.add(provisionalresult_pizza_1, "13,3");
		content.add(provisionalresult_pizza_2, "13,4");
		content.add(endresult_pizza_1, "16,3,17,3");//"15,3,18,3"
		content.add(endresult_pizza_2, "16,4,17,4");//"15,4,18,4"

			btn_continue.addActionListener(this);
			btn_continue.addKeyListener(this);
			btn_continue.setFocusTraversalKeysEnabled(false);
		content.add(btn_continue, "19,0,c,b");
		
			btn_end.addActionListener(this);
			btn_end.addKeyListener(this);
		content.add(btn_end, "19,2,c,b");
		content.add(pdsp, "19,3,19,4");
		
		if(operator.equals("+")){
			points = owner.points_addFractions;
		}else{
			points = owner.points_subtractFractions;
		}
		pdsp.setText(String.valueOf(points));

			lb_info.setText(lang.Messages.getString("you_just_need_nb_and_enter"));
		content.add(lb_info, "0,5,20,5");

		makeProblem();

		String[] ar_howto = {
				"howto_nb_and_enter","howto_option_type_of_exercise", "howto_option_reducing"
			};
		new UsabilityDialog(ar_howto);//new UsabilityDialog
	}//Konstruktor

	/**
	 * 
	 *
	 */
	protected void makeProblem(){
		if(operator.equals("+")){
			fraction = new FractionMaker(10); //der Parameter setzt die exclusive Obergrenze der Zahlen des Bruches
			numerator1 = fraction.getNumerator_1();
			denominator1 = fraction.getDenominator_1();
			numerator2 = fraction.getNumerator_2();
			denominator2 = fraction.getDenominator_2();
		}else if (operator.equals("-")){
			do{ 
				fraction = new FractionMaker(10);
				numerator1 = fraction.getNumerator_1();
				denominator1 = fraction.getDenominator_1();
				numerator2 = fraction.getNumerator_2();
				denominator2 = fraction.getDenominator_2();
			}while(!((float)numerator1/denominator1 > (float)numerator2/denominator2));
		}
		//Testzahlen
		//numerator1 = 3; denominator1 = 5; numerator2 = 6; denominator2 = 10;
		//numerator1 = 8; denominator1 = 10; numerator2 = 1; denominator2 = 9;
		//numerator1 = 6; denominator1 = 7; numerator2 = 7; denominator2 = 10;
		tf_numerator1.setText(String.valueOf(numerator1));
		tf_denominator1.setText(String.valueOf(denominator1));
		//System.out.println("zeichne AnfangsPizza1");
		pizza_1.drawPizzaAsCircle(numerator1, denominator1, Color.yellow);
		tf_numerator2.setText(String.valueOf(numerator2));
		tf_denominator2.setText(String.valueOf(denominator2));
		//System.out.println("zeichne AnfangsPizza2");
		pizza_2.drawPizzaAsCircle(
			numerator1, denominator1,
			numerator2, denominator2,
			Color.yellow
		);
		tf_calculation_denominator1.setEditable(true);
		tf_calculation_denominator1.requestFocusInWindow();
	}

	/**
	 * 
	 *
	 */
	protected void nextProblem(){
		clearTextFields();
		lb_number_one.setText("");
		tf_provisionalresult_denominator.setText("");
		calculation_pizza_1.noPizzaAsCircle();
		calculation_pizza_2.noPizzaAsCircle();
		provisionalresult_pizza_1.noPizzaAsCircle();
		provisionalresult_pizza_2.noPizzaAsCircle();
		endresult_pizza_1.noPizzaAsCircle();
		endresult_pizza_2.noPizzaAsCircle();
		if (bl_randomProblem == true){
			makeProblem();
			tf_calculation_denominator1.setEditable(true);
			tf_calculation_denominator1.requestFocusInWindow();
		}else{
			tf_numerator1.setEditable(true);
			tf_numerator1.requestFocusInWindow();
			//leerpizzen
			pizza_1.noPizzaAsCircle();
			pizza_2.noPizzaAsCircle();
		}
	}//private void nextProblem(){
	
	/**
	 * 
	 * @param hn
	 * @return
	 */
	private boolean checkCommonDenominator(int hn){
		if (hn != 0  & hn % denominator1 == 0 & hn % denominator2 == 0){
			return true;
		} else {return false;}
	}//checkCommonDenominator()
	
	/**
	 * 
	 */
	public void actionPerformed (ActionEvent e) {
		Object obj = e.getSource();
		if (obj == btn_continue){
			nextProblem();
		}else if (obj == btn_end){
			close_it();
		}else if (obj == rb_random){
			//System.out.println("rb_random");
			//the program is in "custom-mode" actually
			//it should switch to "random-mode"
			bl_randomProblem = true;
			tf_numerator1.setText("");
			nextProblem();
		}else if (obj == rb_custom){
			//System.out.println("rb_custom");
			//the program is in "random-mode" actually
			//it should switch to "custom-mode"
			bl_randomProblem = false;
			nextProblem();
			tf_calculation_denominator1.setEditable(false);
			tf_numerator1.setEditable(true);
			tf_numerator1.requestFocusInWindow();
		}else if (obj == rb_reducing){
			//System.out.println("rb_reducing");
			//the program is in "no reduce mode" actually
			//it should switch to "reduce-mode"
			bl_with_reducing = true;
			nextProblem();
		}else if (obj == rb_no_reducing){
			//System.out.println("rb_no_reducing");
			//the program is in "reduce mode" actually
			//it should switch to "no reduce-mode"
			bl_with_reducing = false;
			nextProblem();
		}
		else if(obj == jmiCreateWorkSheet){
			if(operator.equals("+")){
				new WorkSheetDialog(ExerciseGenerator.ADD_FRACTIONS, lang.Messages.getString("add_fractions"));
			}else{
				new WorkSheetDialog(ExerciseGenerator.SUBTRACT_FRACTIONS, lang.Messages.getString("minus_fractions"));
			}
		}
		else if(obj == jmiHelp){
			if(operator.equals("+")){
				new HelpStarter(
						lang.Messages.getLocale().toString(),
						"add"
				);
			}else{
				new HelpStarter(
						lang.Messages.getLocale().toString(),
						"subtract"
				);
			}
			
		}
	}//actionPerformed
	
	/**
	 * 
	 */
	public void keyPressed(KeyEvent event){
		Object obj = event.getSource();
		int key = event.getKeyCode();
		lb_info.setText("");
		if (obj == tf_numerator1 & key == KeyEvent.VK_ENTER){
			if ( readInputInt(tf_numerator1) != null){
				numerator1 = readInputInt(tf_numerator1);
				tf_numerator1.setEditable(false);
				tf_denominator1.setEditable(true);
				tf_denominator1.requestFocusInWindow();
			}
		}else if(obj == tf_denominator1 & key == KeyEvent.VK_ENTER){
			if ( readInputInt(tf_denominator1) != null){
				int pn = readInputInt(tf_denominator1);
				if(pn <= numerator1){
					lb_info.setText(lang.Messages.getString("dn_must_be_bigger"));
					tf_denominator1.setText("");
					tf_denominator1.setEditable(false);
					tf_numerator1.setText("");
					tf_numerator1.setEditable(true);
					tf_numerator1.requestFocusInWindow();
				}else{
					denominator1 = pn;
					tf_denominator1.setEditable(false);
					pizza_1.drawPizzaAsCircle(numerator1, denominator1, Color.yellow);
					tf_numerator2.setEditable(true);
					tf_numerator2.requestFocusInWindow();
				}
			}//if ( readInputNumber() != null)
		}else if (obj == tf_numerator2 & key == KeyEvent.VK_ENTER){
			if ( readInputInt(tf_numerator2) != null){
				numerator2 = readInputInt(tf_numerator2);
				tf_numerator2.setEditable(false);
				tf_denominator2.setEditable(true);
				tf_denominator2.requestFocusInWindow();
			}//if ( readInputNumber() != null)
		}else if(obj == tf_denominator2 & key == KeyEvent.VK_ENTER){
			if ( readInputInt(tf_denominator2) != null){
				int pn = readInputInt(tf_denominator2);
				if(pn <= numerator2){
					lb_info.setText(lang.Messages.getString("dn_must_be_bigger"));
					tf_denominator2.setText("");
					tf_denominator2.setEditable(false);
					tf_numerator2.setText("");
					tf_numerator2.setEditable(true);
					tf_numerator2.requestFocusInWindow();
				}else{
					denominator2 = pn;
					if(operator.equals("+")){
						pizza_2.drawPizzaAsCircle(numerator1, denominator1, numerator2, denominator2, Color.yellow);
						tf_denominator2.setEditable(false);
						tf_calculation_denominator1.setEditable(true);
						tf_calculation_denominator1.requestFocusInWindow();
					}else if (operator.equals("-")){
						if((float)numerator1/denominator1 <= (float)numerator2/denominator2){
							lb_info.setText(lang.Messages.getString("frac2_must_be_smaller"));
							tf_denominator2.setText("");
							tf_denominator2.setEditable(false);
							tf_numerator2.setText("");
							tf_numerator2.setEditable(true);
							tf_numerator2.requestFocusInWindow();
						}else{
							pizza_2.drawMinusPizzaAsCircle(
									numerator1, denominator1, numerator2, denominator2, Color.yellow
							);
							tf_denominator2.setEditable(false);
							tf_calculation_denominator1.setEditable(true);
							tf_calculation_denominator1.requestFocusInWindow();
						}
					}else{
						//ERROR es geht nur + oder -
					}
				}
			}//if ( readInputNumber() != null)
		}else if (obj == tf_calculation_denominator1 & key == KeyEvent.VK_ENTER){
			if ( readInputInt(tf_calculation_denominator1) != null){
				int rn = readInputInt(tf_calculation_denominator1);
				if (!checkCommonDenominator(rn)){
					// common denominator is NOT right
					calculation_pizza_1.drawPizzaAsCircle(
						common_denominator,
						common_denominator,
						Color.red
					);
					calculation_pizza_2.drawPizzaAsCircle(
						common_denominator,
						common_denominator,
						Color.red
					);
					tf_calculation_denominator1.setText("");
					lb_info.setText(lang.Messages.getString("cd_is_leastcommonmultiple"));
				}else{
					//System.out.println("common denominator is right");
					if(rn != JFractionLab.leastComonMultiple(denominator1, denominator2)){
						new WannaFindBestCommonDenominator(this);
					}else{
						continueWithThisCommonDenominator(rn);
					}
					if(bl_WannaFindBestCommonDenominator_AnswerIsYes){
						bl_WannaFindBestCommonDenominator_AnswerIsYes = false;
						tf_calculation_denominator1.setText("");
						tf_calculation_denominator1.requestFocusInWindow();
					}else {
						continueWithThisCommonDenominator(rn);
					}
				}
			}//if ( readInputNumber() != null)
		}else if (obj == tf_calculation_numerator1 & key == KeyEvent.VK_ENTER){
			if ( readInputInt(tf_calculation_numerator1) != null){
				int cn1 = readInputInt(tf_calculation_numerator1);
				int xx = numerator1 * common_denominator / denominator1;
				if(cn1 != xx ){
					//System.out.println("xx nicht OK");
					calculation_pizza_1.drawPizzaAsCircle(
						cn1,
						common_denominator,
						Color.red
					);
					tf_calculation_numerator1.setText("");
					lb_info.setText(lang.Messages.getString("wrong_numerator"));
					tf_calculation_numerator1.requestFocusInWindow();
				}else{
					//Zahl ist OK
					calculation_numerator1 = cn1;
					calculation_pizza_1.drawPizzaAsCircle(
						calculation_numerator1,
						common_denominator,
						Color.yellow
					);
					tf_calculation_numerator1.setEditable(false);
					tf_calculation_numerator2.setEditable(true);
					tf_calculation_numerator2.requestFocusInWindow();
				}
			}//if ( readInputNumber() != null)
		}else if (obj == tf_calculation_numerator2 & key == KeyEvent.VK_ENTER){
			if ( readInputInt(tf_calculation_numerator2) != null){
				int cn2 = readInputInt(tf_calculation_numerator2);
				int xx =  numerator2 * common_denominator / denominator2;
				if(cn2 != xx){
					if(operator.equals("+")){
					calculation_pizza_2.drawPizzaAsCircle(
						calculation_numerator1,
						common_denominator,
						cn2,
						common_denominator,
						Color.red
					);
				}else if(operator.equals("-")){
					calculation_pizza_2.drawMinusPizzaAsCircle(
						calculation_numerator1,
						common_denominator,
						cn2,
						common_denominator,
						Color.red
					);
				}
				tf_calculation_numerator2.setText("");
				lb_info.setText(lang.Messages.getString("wrong_numerator"));
				//tf_calculation_numerator2.requestFocusInWindow();
				}else{
					//Zahl ist OK
					calculation_numerator2 = cn2;
					if(operator.equals("+")){
						calculation_pizza_2.drawPizzaAsCircle(
							calculation_numerator1,
							common_denominator,
							calculation_numerator2,
							common_denominator,
							Color.yellow
						);
					}else if(operator.equals("-")){
						calculation_pizza_2.drawMinusPizzaAsCircle(
							calculation_numerator1,
							common_denominator,
							calculation_numerator2,
							common_denominator,
							Color.yellow
						);
					}
					tf_calculation_numerator2.setEditable(false);
					tf_provisionalresult_numerator.setEditable(true);
					tf_provisionalresult_numerator.requestFocusInWindow();
				}
			}//if ( readInputNumber() != null)
		}else if (obj == tf_provisionalresult_numerator & key == KeyEvent.VK_ENTER){
			tf_provisionalresult_numerator_FocusLost();
		}else if (obj == tf_endresult_numerator & key == KeyEvent.VK_ENTER){
			if ( readInputInt(tf_endresult_numerator) != null){
				endresult_numerator = readInputInt(tf_endresult_numerator);
				//it is only checked that the numerator is not too big
				//anything else is checked in the "tf_endresult_denominator"-chapter
				int num = 0;
				if (provisionalresult_numerator > common_denominator){//result is bigger than 1
					num = provisionalresult_numerator - common_denominator;
				}else{
					num = provisionalresult_numerator;
				}
				if(endresult_numerator>num){
					tf_endresult_numerator.setText("");
					lb_info.setText(lang.Messages.getString("numerator_is_too_big"));
				}else{
					if(common_denominator % (num/endresult_numerator) != 0){
						tf_endresult_numerator.setText("");
						lb_info.setText(lang.Messages.getString("reduce_by_a_common_denominator"));
					}else{
						tf_endresult_numerator.setEditable(false);
						tf_endresult_denominator.setEditable(true);
						tf_endresult_denominator.requestFocusInWindow();
					}
				}
			}//if ( readInputNumber() != null)
		}else if (obj == tf_endresult_denominator & key == KeyEvent.VK_ENTER){
			if ( readInputInt(tf_endresult_denominator) != null){
				int endresult_denominator = readInputInt(tf_endresult_denominator);
				int ggt = JFractionLab.greatestCommonDivisor(provisionalresult_numerator , common_denominator);
				
				//denominator : reduction is optimal
				if(endresult_denominator == common_denominator / ggt){
					if(!endresult_Numerator_isOK("optimal", endresult_denominator, ggt)){
						endresult_OptimalReduction_WrongNumerator(endresult_denominator);
					}else{
						endresult_OptimalReduction_RightNumerator(endresult_denominator);
					}
				}
				
				//denominator : reduction is good but not optimal
				else if(endresult_denominator == common_denominator || common_denominator % endresult_denominator == 0	){
					if(!endresult_Numerator_isOK("good_but_not_optimal", endresult_denominator, ggt)){
						endresult_GoodReduction_WrongNumerator(endresult_denominator);
					}else{
						endresult_GoodReduction_RightNumerator(endresult_denominator);
					}
				}
				
				//denominator : reduction is wrong
				else{
					endresult_BadReduction(endresult_denominator);
				}//if(endresult_denominator ...
			
			}//if ( readInputNumber(tf_endresult_denominator) != null)
		}else if(obj == btn_continue & key == KeyEvent.VK_ENTER){
			nextProblem();
		}else if(obj == btn_end & key == KeyEvent.VK_ENTER){
			close_it();
		}
	}//keyPressed
	public void keyTyped(KeyEvent event){}
	public void keyReleased(KeyEvent event){}
	
	private void continueWithThisCommonDenominator(int nb){
		common_denominator = nb;
		//PizzaweissmitSegmenten;
		calculation_pizza_1.drawPizzaAsCircle(
			common_denominator,
			common_denominator,
			Color.white
		);
		//PizzaweissmitSegmenten;
		calculation_pizza_2.drawPizzaAsCircle(
			common_denominator,
			common_denominator,
			Color.white
		);
		tf_calculation_denominator2.setText(tf_calculation_denominator1.getText());
		tf_provisionalresult_denominator.setText(tf_calculation_denominator1.getText());
		tf_calculation_denominator1.setEditable(false);
		tf_calculation_numerator1.setEditable(true);
		tf_calculation_numerator1.requestFocusInWindow();
	}
	private void tf_provisionalresult_numerator_FocusLost(){
		if ( readInputInt(tf_provisionalresult_numerator) != null){
			int prn = readInputInt(tf_provisionalresult_numerator);
			int result = 0;
			if(operator.equals("+")){
				result = calculation_numerator1 + calculation_numerator2;
			}else if(operator.equals("-")){
				result = calculation_numerator1 - calculation_numerator2;
			}else{
				//ERROR
			}
			if(prn != result){
				//ergebnis falsch
				int falscherZaehler = prn;
				if(falscherZaehler > common_denominator){
					provisionalresult_pizza_1.drawPizzaAsCircle(
						common_denominator,
						common_denominator,
						Color.red
					);
					provisionalresult_pizza_2.drawPizzaAsCircle(
						falscherZaehler - common_denominator,
						common_denominator,
						Color.red
					);
				}else{
					provisionalresult_pizza_1.drawPizzaAsCircle(
						falscherZaehler,
						common_denominator,
						Color.red
					);
				}
				tf_provisionalresult_numerator.setText("");
				if(operator.equals("+")){
					lb_info.setText(lang.Messages.getString("add_nums"));
				}else if(operator.equals("-")){
					lb_info.setText(lang.Messages.getString("sub_the_nums"));
				}else{
					//ERROR
				}
				tf_provisionalresult_numerator.requestFocusInWindow();
			}else{
				//ergebnis richtig
				if(bl_with_reducing){
					reduce_this_problem(prn);
				}else{ //ohne kuerzen
					System.out.println("ggt = "+JFractionLab.greatestCommonDivisor(prn, common_denominator));
					System.out.println("prn != common_denominator ?? =" +
							prn+", "+common_denominator);
					if(
							JFractionLab.greatestCommonDivisor(prn, common_denominator) != 1
							||
							prn > common_denominator
						){
						if(prn == common_denominator ){
						}else{
							new WannaReduceQuestion(this);
						}
					}
					if(bl_wannaReduceQuestion_AnswerIsYes){
						reduce_this_problem(prn);
						bl_wannaReduceQuestion_AnswerIsYes = false;
					}else{
						if(bl_randomProblem){points++;};
						lb_info.setText(lang.Messages.getString("that_is_right"));
						pdsp.setText(String.valueOf(points));
						if(operator.equals("+")){
							owner.setPoints(points, "addFractions");	
						}else{
							owner.setPoints(points, "subtractFractions");
						}
						tf_provisionalresult_numerator.setEditable(false);
						lb_equalsign_endresult.setText("");
						btn_continue.requestFocusInWindow();
					}
				}
			}
		}//if ( readInputNumber() != null)
	}//private void tf_provisionalresult_numerator_FocusLost(){

	private void reduce_this_problem(int prn){
		provisionalresult_numerator = prn;
		tf_provisionalresult_numerator.setEditable(false);
		tf_endresult_numerator.setEditable(true);
		tf_endresult_numerator.requestFocusInWindow();
		//Wenn Ergebnis > 1
		if(provisionalresult_numerator > common_denominator){
			provisionalresult_pizza_1.drawPizzaAsCircle(
				common_denominator,
				common_denominator,
				Color.yellow
			);
			provisionalresult_pizza_2.drawPizzaAsCircle(
				provisionalresult_numerator - common_denominator,
				common_denominator,
				Color.yellow
			);
			lb_number_one.setText("1");
			tf_endresult_numerator.requestFocusInWindow();
		}else{ //Ergebnis ist nicht groesser als 1
			//Wenn das Ergebnis nicht weiter zu kuerzen ist
			if(JFractionLab.greatestCommonDivisor(provisionalresult_numerator, common_denominator) == 1){
				if(bl_randomProblem){points++;};
				lb_info.setText(lang.Messages.getString("that_is_right"));
				pdsp.setText(String.valueOf(points));
				if(operator.equals("+")){
					owner.setPoints(points, "addFractions");	
				}else{
					owner.setPoints(points, "subtractFractions");
				}
				tf_endresult_numerator.setEditable(false);
				lb_equalsign_endresult.setText("");
				btn_continue.requestFocusInWindow();
			}
			provisionalresult_pizza_1.drawPizzaAsCircle(
				provisionalresult_numerator,
				common_denominator,
				Color.yellow
			);
		}//ergebnis <> 1
		//Wenn Ergebnis = 1
		if (provisionalresult_numerator == common_denominator ){
			if(bl_randomProblem){points++;};
			lb_info.setText(lang.Messages.getString("that_is_right"));
			pdsp.setText(String.valueOf(points));
			if(operator.equals("+")){
				owner.setPoints(points, "addFractions");	
			}else{
				owner.setPoints(points, "subtractFractions");
			}
			tf_endresult_numerator.setEditable(false);
			lb_equalsign_endresult.setText("");
			btn_continue.requestFocusInWindow();
		}
	}
	
	private boolean endresult_Numerator_isOK(String str, int endresult_denominator, int ggt){
		if(str.equals("optimal")){ //optimalgekürzt
			if(provisionalresult_numerator > common_denominator){
				if(endresult_numerator == (provisionalresult_numerator - common_denominator) / ggt){
					//System.out.println("z>n, numeratorOK = true;");
					return true;
				}else{return false;}
			}else{
				if(endresult_numerator == provisionalresult_numerator / ggt){
					//System.out.println("numeratorOK = true;");
					return true;
				}else{return false;}
			}
		}else if(str.equals("good_but_not_optimal")){//richtig aber nicht optimal gekuerzt
			int kuerzungsFaktor = common_denominator / endresult_denominator;
			if(provisionalresult_numerator > common_denominator){
				if(endresult_numerator == (provisionalresult_numerator - common_denominator) / kuerzungsFaktor){
					//System.out.println("z>n, numeratorOK = true;");
					return true;
				}else{return false;}
			}else{
				if(endresult_numerator == provisionalresult_numerator / kuerzungsFaktor){
					//System.out.println("numeratorOK = true;");
					return true;
				}else{return false;}
			}
		}else{return false;}
	}
	
	private void endresult_OptimalReduction_WrongNumerator(int endresult_denominator){
		System.out.println("Zaehler ist nicht OK");
		if(provisionalresult_numerator > common_denominator){
			endresult_pizza_1.drawPizzaAsCircle(
				endresult_denominator,
				endresult_denominator,
				Color.white
			);
			endresult_pizza_2.drawPizzaAsCircle(
				endresult_denominator,
				endresult_denominator,
				Color.white
			);
		}else{
			endresult_pizza_1.drawPizzaAsCircle(
				endresult_denominator,
				endresult_denominator,
				Color.white
			);
		}//ergebnis > 1
		tf_endresult_denominator.setText("");
		lb_info.setText(lang.Messages.getString("n_and_dn_do_not_match"));
	
	}//endresult_OptimalReduction_WrongNumerator()
	
	private void endresult_OptimalReduction_RightNumerator(int endresult_denominator){
		if(provisionalresult_numerator > common_denominator){
			endresult_pizza_1.drawPizzaAsCircle(
				endresult_denominator,
				endresult_denominator,
				Color.yellow
			);
			endresult_pizza_2.drawPizzaAsCircle(
				endresult_numerator,
				endresult_denominator,
				Color.yellow
			);
		}else{
			endresult_pizza_1.drawPizzaAsCircle(
				endresult_numerator,
				endresult_denominator,
				Color.yellow
			);
		}//ergebnis > 1
		if(bl_randomProblem){points++;points++;};
		lb_info.setText(lang.Messages.getString("that_is_right"));
		pdsp.setText(String.valueOf(points));
		if(operator.equals("+")){
			owner.setPoints(points, "addFractions");	
		}else{
			owner.setPoints(points, "subtractFractions");
		}
		btn_continue.requestFocusInWindow();
	}//endresult_OptimalReduction_RightNumerator()
	
	private void endresult_GoodReduction_WrongNumerator(int endresult_denominator){
		if(provisionalresult_numerator > common_denominator){
			endresult_pizza_1.drawPizzaAsCircle(
				endresult_denominator,
				endresult_denominator,
				Color.white
			);
			endresult_pizza_2.drawPizzaAsCircle(
				endresult_denominator,
				endresult_denominator,
				Color.white
			);
		}else{
			endresult_pizza_1.drawPizzaAsCircle(
				endresult_denominator,
				endresult_denominator,
				Color.white
			);
		}//ergebnis > 1
		tf_endresult_numerator.setText("");
		tf_endresult_denominator.setText("");
		tf_endresult_denominator.setEditable(false);
		tf_endresult_numerator.setEditable(true);
		tf_endresult_denominator.setText("");
		lb_info.setText(lang.Messages.getString("n_and_dn_do_not_match"));
		tf_endresult_numerator.requestFocusInWindow();
	}//endresult_GoodReduction_WrongNumerator()
	
	private void endresult_GoodReduction_RightNumerator(int endresult_denominator){
		if(provisionalresult_numerator > common_denominator){
			endresult_pizza_1.drawPizzaAsCircle(
				endresult_denominator,
				endresult_denominator,
				Color.pink
			);
			endresult_pizza_2.drawPizzaAsCircle(
				endresult_numerator,
				endresult_denominator,
				Color.pink
			);
		}else{
			endresult_pizza_1.drawPizzaAsCircle(
				endresult_numerator,
				endresult_denominator,
				Color.pink
			);
		}//ergebnis > 1
		//tf_endresult_numerator.setText("");
		//tf_endresult_denominator.setText("");
		lb_info.setText(lang.Messages.getString("reduce_better"));
		tf_endresult_denominator.setEditable(false);
		tf_endresult_numerator.setEditable(true);
		tf_endresult_numerator.requestFocusInWindow();
	}//endresult_GoodReduction_RightNumerator()
	
	private void endresult_BadReduction(int endresult_denominator){
		if(provisionalresult_numerator > common_denominator){
			endresult_pizza_1.drawPizzaAsCircle(1,1,Color.white);
			endresult_pizza_2.drawPizzaAsCircle(1,1,Color.white);
		}else{
			endresult_pizza_1.drawPizzaAsCircle(1,1,Color.white);
		}//ergebnis > 1
		tf_endresult_numerator.setText("");
		tf_endresult_denominator.setText("");
		tf_endresult_denominator.setEditable(false);
		tf_endresult_numerator.setEditable(true);
		lb_info.setText(lang.Messages.getString("wrong_denominator"));
		tf_endresult_numerator.requestFocusInWindow();
	}//endresultBadReduction()

}//class

