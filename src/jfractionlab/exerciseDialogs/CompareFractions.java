/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package jfractionlab.exerciseDialogs;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.Container;
import java.awt.HeadlessException;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JLabel;

import jfractionlab.FractionMaker;
import jfractionlab.JFractionLab;
import jfractionlab.displays.FractionAsCircle;
import jfractionlab.displays.FractionLine;
import jfractionlab.jflDialogs.UsabilityDialog;




public class CompareFractions extends ExerciseDialog implements ActionListener{
	static final long serialVersionUID = JFractionLab.serialVersionUID;

	//GUI
	private JLabel lb_numerator_1 = new JLabel("", JLabel.CENTER);
	private JLabel lb_denominator_1 = new JLabel("", JLabel.CENTER);
	private JLabel lb_numerator_2 = new JLabel("", JLabel.CENTER);
	private JLabel lb_denominator_2 = new JLabel("", JLabel.CENTER);
	private FractionAsCircle pp_pizza_1 = new FractionAsCircle();
	private FractionAsCircle pp_pizza_2 = new FractionAsCircle();
	private JButton btn_bigger;
	private JButton btn_equal;
	private JButton btn_smaller;
	private JButton btn_tipp;
	//public Locale mylocale;
	//Zahlen
	private FractionMaker fraction;
	private int numerator_1;
	private int denominator_1;
	private float sizeOfFraction_1;
	private int numerator_2;
	private int denominator_2;
	private float sizeOfFraction_2;
	private boolean virgin = true;
	private boolean isVisible = true;
	private boolean solved = false;


	/**
	 * 
	 * @param owner
	 * @param lx
	 * @param ly
	 * @param sx
	 * @param sy
	 * @throws HeadlessException
	 */
	public CompareFractions(JFractionLab owner, int lx, int ly, int sx, int sy) throws HeadlessException {
		super(lx, ly, sx, sy);
		setTitle(lang.Messages.getString("compare_the_fractions"));
		btn_continue = new JButton(lang.Messages.getString("continue"));
		btn_tipp = new JButton(lang.Messages.getString("Tipp"));
		btn_end =  new JButton(lang.Messages.getString("end"));
		btn_bigger = new JButton(">");
		btn_equal = new JButton("=");
		btn_smaller = new JButton("<");
		this.owner = owner;
		//Menu
		rb_visible.addActionListener(this);
		jmOptions.add(rb_visible);
		rb_invisible.addActionListener(this);
		jmOptions.add(rb_invisible);
		jmb.add(jmOptions);
		jmiHelp.addActionListener(this);
		jmb.add(jmHelp);
		setJMenuBar(jmb);
		
		double sizes[][] = {{	
			// Spalten
			TableLayout.FILL,
			0.06,
			TableLayout.FILL,
			0.05,0.05,
			TableLayout.FILL,
			0.06,
			TableLayout.FILL,
			0.10,
			12
		},{	
			//Zeilen
			0.10,
			0.033,
			0.033,
			0.033,
			0.10,
			0.30,
			0.30,
			TableLayout.FILL
		}};

		Container content = getContentPane();
        	content.setLayout(new TableLayout(sizes));
		content.setBackground(Color.white);		
		content.add(lb_numerator_1, "1,0,1,1,c,b");
		FractionLine bs_1 = new FractionLine();
		content.add(bs_1, "1,2");
		content.add(lb_denominator_1, "1,3,1,4,c,t");
		content.add(pp_pizza_1, "0,5,3,6");
		
			btn_bigger.addActionListener(this);
		content.add(btn_bigger, "3,0,4,0,f,b");
			btn_equal.addActionListener(this);
		content.add(btn_equal, "3,1,4,3,f,c");
			btn_smaller.addActionListener(this);
		content.add(btn_smaller, "3,4,4,4,f,t");
		
		content.add(lb_numerator_2, "6,0,6,1,c,b");
		FractionLine bs_2 = new FractionLine();
		content.add(bs_2, "6,2");
		content.add(lb_denominator_2, "6,3,6,4,c,t");
		
		if(isVisible){
			pp_pizza_1.drawPizzaAsCircle(numerator_1, denominator_1, Color.yellow);
			pp_pizza_2.drawPizzaAsCircle(numerator_2, denominator_2, Color.yellow);
		}
		content.add(pp_pizza_2, "4,5,7,6");
		
			btn_continue.addActionListener(this);
		content.add(btn_continue, "8,0,f,b");
			btn_tipp.addActionListener(this);
			btn_tipp.setEnabled(false);
		content.add(btn_tipp, "8,1,8,3,f,c");
			
		
			btn_end.addActionListener(this);
		content.add(btn_end, "8,4,f,t");
			points = owner.points_compareFractions;
			pdsp.setText(String.valueOf(points));
		content.add(pdsp, "8,6,8,7");
			lb_info.setFont(JFractionLab.infofont);
			lb_info.setText(lang.Messages.getString("use_the_mouse"));
		content.add(lb_info, "0,7,7,7");
		makeProblem();
		String[] ar_howto = {"howto_click_bigger_smaller","howto_option_invisible"};
		new UsabilityDialog(ar_howto);//new UsabilityDialog
		
	
		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher(){
			public boolean dispatchKeyEvent(KeyEvent e){
				boolean keyHandled = false;
				if(e.getID() == KeyEvent.KEY_PRESSED){
					if(e.getKeyChar() == '>'){
//						System.out.println("groesser");
						biggerAction();
					}else if(e.getKeyChar() == '<'){
//						System.out.println("kleiner");
						smallerAction();
					}else if(e.getKeyChar() == '='){
//						System.out.println("gleich");
						equalAction();
					}else if(e.getKeyCode() == KeyEvent.VK_ENTER){
						lb_info.setText("");
						makeProblem();
					}
				}
				return keyHandled;
			}
		});
	}//Konstruktor
	
	protected void nextProblem(){}
	
	/**
	 * 
	 *
	 */
	protected void makeProblem(){
		//System.out.println("makeProblem");
		fraction = new FractionMaker();
		int ran;
		ran = Math.abs(JFractionLab.ran.nextInt()%3000);
		if (ran < 1000){
			fraction.mkTwoFractionsWithEqualNumerators(10);//gleicheZaehler
			
			numerator_1 = fraction.getNumerator_1();
			numerator_2 = numerator_1;
			
			denominator_1 = fraction.getDenominator_1();
			denominator_2 = fraction.getDenominator_2();
			
		}else if (ran >= 1000 && ran < 2000){
			fraction.mkTwoFractionsWithEqualDenominators(10);//gleiche Nenner
			
			numerator_1 = fraction.getNumerator_1();
			numerator_2 = fraction.getNumerator_2();
			
			denominator_1 = fraction.getDenominator_1();
			denominator_2 = denominator_1;
			
		}else if (ran >= 2000){
			fraction.mkTwoFractions(10);
			numerator_1 = fraction.getNumerator_1();
			denominator_1 = fraction.getDenominator_1();
			numerator_2 = fraction.getNumerator_2();
			denominator_2 = fraction.getDenominator_2();
		}
		//numbers to test
//		numerator_1 = 1;
//		denominator_1 = 2;
//		numerator_2 = 1;
//		denominator_2 = 4;
		solved = false;
		virgin = true;
		sizeOfFraction_1 = (float)numerator_1/denominator_1;
		sizeOfFraction_2 = (float)numerator_2/denominator_2;
		lb_numerator_1.setText(String.valueOf(numerator_1));
		lb_denominator_1.setText(String.valueOf(denominator_1));
		lb_numerator_2.setText(String.valueOf(numerator_2));
		lb_denominator_2.setText(String.valueOf(denominator_2));
		lb_info.setText(lang.Messages.getString("bigger_equal_or_smaller"));
		pp_pizza_1.noPizzaAsCircle();
		pp_pizza_2.noPizzaAsCircle();
		if(isVisible){
			pp_pizza_1.drawPizzaAsCircle(numerator_1, denominator_1, Color.yellow);
			pp_pizza_2.drawPizzaAsCircle(numerator_2, denominator_2, Color.yellow);
		}
	}//maleProblem
	
	/**
	 * 
	 *
	 */
	private void it_is_ok(String str){
		//System.out.println("isVisible/virgin/solved : "
		//	+String.valueOf(isVisible) 
		//	+"/" 
		//	+String.valueOf(virgin)
		//	+"/" 
		//	+String.valueOf(solved)
		//);
		if (solved){
			lb_info.setText(lang.Messages.getString("already_solved")+" "+lang.Messages.getString("press_enter"));
		}else{
			if(!isVisible){
				if(virgin){//unsichtbar und ohne tipp
					points++;
					points++;
				}else{//unsichtbar aber mit tipp
					points++;
				}
			}else{//sichtbar
				points++;
			}
			lb_info.setText("\""+str+"\" : "+lang.Messages.getString("that_is_right"));
			pdsp.setText(String.valueOf(points));
			owner.setPoints(points, "compareFractions");
			pp_pizza_1.drawPizzaAsCircle(numerator_1, denominator_1, Color.yellow);
			pp_pizza_2.drawPizzaAsCircle(numerator_2, denominator_2, Color.yellow);
			solved = true;
		}//if
	}//it_is_ok
	
	private void smallerAction(){
			if (sizeOfFraction_1 < sizeOfFraction_2){
			it_is_ok("<");
		}else{
			lb_info.setText(lang.Messages.getString("left_is_not_smaller"));
		}
		virgin = false;
	}
	private void biggerAction(){
		if (sizeOfFraction_1 > sizeOfFraction_2){
			it_is_ok(">");
		}else{
			lb_info.setText(lang.Messages.getString("left_is_not_bigger"));
		}
		virgin = false;
	}
	private void equalAction(){
		if (sizeOfFraction_1 == sizeOfFraction_2){
			it_is_ok("=");
		}else{
			lb_info.setText(lang.Messages.getString("not_equal"));
		}
		virgin = false;
	}
	/**
	 * 
	 */
	public void actionPerformed (ActionEvent e) {
	Object obj = e.getSource();
        if (obj == btn_continue){
			lb_info.setText("");
			makeProblem();
		}else if (obj == btn_tipp){
			pp_pizza_1.drawPizzaAsCircle(numerator_1, denominator_1, Color.yellow);
			pp_pizza_2.drawPizzaAsCircle(numerator_2, denominator_2, Color.yellow);
			virgin = false;
		}else if (obj == btn_end){
			setVisible(false);
	        	dispose();
		}else if (obj == btn_bigger){
			biggerAction();
		}else if (obj == btn_equal){
			equalAction();
		}else if (obj == btn_smaller){
			smallerAction();
		}else if (obj == rb_visible){
			//System.out.println("rb_visible");
			//the program is in "Invisible-mode" actually
			//but it should switch to "Visible-mode"
			pp_pizza_1.drawPizzaAsCircle(numerator_1, denominator_1, Color.yellow);
			pp_pizza_2.drawPizzaAsCircle(numerator_2, denominator_2, Color.yellow);
			btn_tipp.setEnabled(false);
			isVisible = true;
		}else if (obj == rb_invisible){
			//System.out.println("rb_invisible");
			//the program is in "Visible-mode" actually
			//it should switch to "Invisible-mode"
			pp_pizza_1.noPizzaAsCircle();
			pp_pizza_2.noPizzaAsCircle();
			btn_tipp.setEnabled(true);
			isVisible = false;
			makeProblem();
		}
	}//actionPerformed
}//class
