/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package jfractionlab;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTextField;


public class MyJTextField extends JTextField implements KeyListener{
	static final long serialVersionUID = JFractionLab.serialVersionUID;

	public MyJTextField(int col){
		setColumns(col);
		setFocusTraversalKeysEnabled(false);
		setHorizontalAlignment(JTextField.CENTER);
		setDumb();
		addKeyListener(this);
	}

	public void setDumb(){
		setText("");
		setEditable(false);
	}

	public void keyPressed(KeyEvent event){
		int key = event.getKeyCode();
		if(key == KeyEvent.VK_TAB){
			JOptionPane.showMessageDialog(
				this,
				lang.Messages.getString("no_tab_press_enter")
			);
			//FIXME das "ursprungsfenster muss erst wieder aktiviert werden, damit es den focus haben kann
//			this.requestFocusInWindow();
		}
	}
	public void keyTyped(KeyEvent event){}
	public void keyReleased(KeyEvent event){}
}//class
