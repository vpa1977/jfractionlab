/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package jfractionlab.jflDialogs;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;

import jfractionlab.JFractionLab;

public class InfoDialog extends JDialog implements ActionListener{
	static final long serialVersionUID = JFractionLab.serialVersionUID;
		private JEditorPane jep_info = new JEditorPane();
		private JButton btn_OK = new JButton("");
		private String fontdefend="</b></font>";
		private String dialogText = "";
		private String dialogTitle = "";
		private int xsize = 500;
		private int ysize = 500;

	public InfoDialog(String title, String txt){
			this.dialogTitle = title;
			this.dialogText = txt;
			setTitle(dialogTitle);
			makeGUI();
		}
		
	public InfoDialog(String title, String txt, int x, int y){
		this.dialogTitle = title;
		this.dialogText = txt;
		this.xsize = x;
		this.ysize = y;
		setTitle(dialogTitle);
		makeGUI();
	}
	
	/**
	 * 
	 *
	 */
	private void makeGUI(){
		double sizes[][] = {{
			10, TableLayout.FILL, 10
		},{
			10, TableLayout.FILL, 50,10
		}}; //Spalten / Zeilen
		
		getRootPane().setDefaultButton(btn_OK);
		Container content = getContentPane();
	        content.setLayout(new TableLayout(sizes));
		content.setBackground(Color.white);
		
		jep_info.setContentType("text/html");
		String str = JFractionLab.jep_fontface + "<b>" + dialogText + fontdefend;
		jep_info.setText(str);
		jep_info.setEditable(false);
		jep_info.setFocusable(false);
		content.add(new JScrollPane(jep_info), "1,1");
		
		btn_OK.addActionListener(this);
		btn_OK.setText(lang.Messages.getString("OK"));
		content.add(btn_OK, "1,2,c,c");

		setModalityType(JDialog.ModalityType.APPLICATION_MODAL);
		setLocation(150, 150);
		setSize(xsize,ysize);
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setVisible(true);
	}//makeGUI

	public void actionPerformed(ActionEvent e){
		Object obj = e.getSource();
		if (obj == btn_OK){
			setVisible(false);
			dispose();
		}
	}//actionPerformed

	public void setDialogText(String dialogText) {
		this.dialogText = dialogText;
	}

	public void setDialogTitle(String dialogTitle) {
		this.dialogTitle = dialogTitle;
	}

	}//class InfoDialog
