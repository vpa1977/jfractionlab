/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package jfractionlab.logtable;

import javax.swing.table.AbstractTableModel;

import jfractionlab.JFractionLab;

class MyTableModel extends AbstractTableModel {
	static final long serialVersionUID = JFractionLab.serialVersionUID;

		public Object[][] data;
		public String[] colnames;
		
		public MyTableModel(Object[][] array_in, String[] colnames){
			this.colnames = colnames;
			data = new Object[array_in.length][colnames.length];
			
			for (int i = 0; i< array_in.length; i++){
//				for(int j = 0; j < colnames.length; j++){
//					data[i][j] = array_in[i][j];
//				}
//			}	
				data[i][0] = array_in[i][0];
				data[i][1] = array_in[i][1];
				data[i][2] = array_in[i][2];
				data[i][3] = array_in[i][3];
				data[i][4] = array_in[i][4];
				data[i][5] = array_in[i][5];
				data[i][6] = array_in[i][6];
				data[i][7] = array_in[i][7];
				data[i][8] = array_in[i][8];
				data[i][9] = array_in[i][9];
				data[i][10] = array_in[i][10];
				data[i][11] = array_in[i][11];
				data[i][12] = array_in[i][12];
				data[i][13] = array_in[i][13];
				data[i][14] = array_in[i][14];
				data[i][15] = array_in[i][15];
			}//for
		}//Konstruktor
		
		/**
		 * 
		 */
		public int getColumnCount() {
			return colnames.length;
		}
		
		/**
		 * 
		 */
		public int getRowCount() {
			return data.length;
		}
		/**
		 * 
		 */
		public String getColumnName(int col) {
			return colnames[col];
		}
		/**
		 * 
		 */
		public Object getValueAt(int row, int col) {
			return data[row][col];
		}
	}//class MyTableModel