package jfractionlab.exerciseGenerator;

import jfractionlab.FractionMaker;

public class GeneratorOfDivisionExercises extends GeneratorObject {

	@Override
	public Exercises getOneExercise(
			int max1,
			int max2
	){
		Exercises ex = new Exercises("");
		int n1;
		int d1;
		int n2;
		int d2;
		String strExercise;
		String strQuickSolution;
		String strLongSolution;
			
		//generate numbers
		FractionMaker fraction = new FractionMaker(max1);
		n1 = fraction.getNumerator_1();
		d1 = fraction.getDenominator_1();
		n2 = fraction.getNumerator_2();
		d2 = fraction.getDenominator_2();
		
		//test numbers
//		n1 = 7; d1 = 6; n2 = 1; d2 = 6;
//		n1 = 20; d1 = 43; n2 = 31; d2 = 97;
//		n1 = 7; d1 = 9; n2 = 4; d2 = 7;
//		n1 = 7; d1 = 9; n2 = 1; d2 = 3;
		
		//generate text of exercise and add it to the arraylist
		strExercise = "{{"+n1+"}over{"+d1+"}} div {{"+n2+"}over{"+d2+"}}";
		ex.getAlExercises().add(strExercise+" ={}");
		
		//generate text of quicksolution and add it to the arraylist
		strQuickSolution = writeReducingTextMultiplyDivide(false, n1, d1, d2, n2);
		ex.getAlSolutions().add(strQuickSolution);

		//generate text of longsolution and add it to the arraylist
		strLongSolution="";
		strLongSolution += writeReducingTextMultiplyDivide(true, n1, d1, d2, n2);
		if(strLongSolution.equals("")){
			strLongSolution = "{{"+n1+"}over{"+d1+"}} cdot {{"+d2+"}over{"+n2+"}}";
		}else{
			strLongSolution = "{{"+n1+"}over{"+d1+"}} cdot {{"+d2+"}over{"+n2+"}} = "+ strLongSolution;
		}
		ex.getAlCalculations().add(
				strExercise+
				" = "+
				strLongSolution+
				" = "+
				strQuickSolution
		);
		return ex;
	}
}