package jfractionlab.exerciseGenerator;

import jfractionlab.FractionMaker;

public class GeneratorOfImproperFractionExercises extends GeneratorObject {

	@Override
	public Exercises getOneExercise(
			int max1,
			int max2
	){
		Exercises ex = new Exercises("");
		int n1;
		int d1;
		String strExercise;
		String strQuickSolution;
		String strLongSolution;
			
		//generate numbers
		FractionMaker fraction = new FractionMaker();
		fraction.mkImproperFraction(max1);
		n1 = fraction.getNumerator_1();
		d1 = fraction.getDenominator_1();

		//generate text of exercise and add it to the arraylist
		strExercise = "{"+n1+"}over{"+d1+"}";
		ex.getAlExercises().add(strExercise+" ={}");
		
		//generate text of quicksolution and add it to the arraylist
		int full_number = n1/d1;
		int rest = n1%d1;
		if(rest == 0){
			strQuickSolution = String.valueOf(full_number);
		}else{
			strQuickSolution = full_number+"{"+rest+"}over{"+d1+"}";
		}
		ex.getAlSolutions().add(strQuickSolution);

		//generate text of longsolution and add it to the arraylist
		if(rest == 0){
			strLongSolution = "";
			ex.getAlCalculations().add(
					strExercise+
					" = "+
					strQuickSolution
			);
		}else{
			strLongSolution = "{"+(full_number*d1)+"}over{"+d1+"}+{"+rest+"}over{"+d1+"}";
			ex.getAlCalculations().add(
					strExercise+
					" = "+
					strLongSolution+
					" = "+
					strQuickSolution
			);
		}

		return ex;
	}

}
