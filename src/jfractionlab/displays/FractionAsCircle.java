/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package jfractionlab.displays;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Arc2D;

import javax.swing.JComponent;

import jfractionlab.JFractionLab;

//public class FractionAsCircle extends Canvas{
public class FractionAsCircle extends JComponent{
	static final long serialVersionUID = JFractionLab.serialVersionUID;
	    private int numerator;
	    private int denominator;
	    private int denominator1;
		private double startangle= 0;
	    private double sector;
	    private int shorterside;
		private Color color = Color.yellow;
		private boolean isVisible = true;

		/**
		 * 
		 */
		public FractionAsCircle(){
		}//konstruktor bildklasse

		/**
		 * 
		 * @param numerator
		 * @param denominator
		 */
		public FractionAsCircle(int numerator, int denominator){
			drawPizzaAsCircle(numerator, denominator, Color.yellow);
		}//konstruktor bildklasse
		
		/**
		 * 
		 * @param z1
		 * @param n1
		 * @param z2
		 * @param n2
		 */
		public FractionAsCircle(int z1, int n1, int z2, int n2){
			drawPizzaAsCircle(z1, n1, z2, n2, Color.yellow);
		}//konstruktor bildklasse

		/**
		 * 
		 * @param numerator
		 * @param denominator
		 * @param color
		 */
		public void drawPizzaAsCircle(int numerator, int denominator, Color color){
			isVisible = true;
			this.numerator = numerator;
			this.denominator = denominator;
			this.color = color;
			if (denominator != 0){
				sector = 360.0/denominator;
			}//if (denominator1!0)
			startangle = 0;
			repaint();
		}    //drawPizzaAsCircle

		/**
		 * 
		 * @param z1
		 * @param n1
		 * @param z2
		 * @param n2
		 * @param color
		 */
		public void drawPizzaAsCircle(int z1, int n1, int z2, int n2, Color color){
			isVisible = true;
			this.denominator1 = n1;
			this.numerator = z2;
			this.denominator = n2;
			this.color = color;
			if (denominator1 != 0){
				sector = 360.0/n2;
			}//if (denominator1!0)
			startangle = 360.0/n1*z1;
			repaint();
		}    //drawPizzaAsCircle
		
		/**
		 * 
		 * @param z1
		 * @param n1
		 * @param z2
		 * @param n2
		 * @param color
		 */
		public void drawMinusPizzaAsCircle(int z1, int n1, int z2, int n2, Color color){
			isVisible = true;
			this.denominator1 = n1;
			this.numerator = z2;
			this.denominator = n2;
			this.color = color;
			if (denominator1 != 0){
				sector = 360.0/n2;
			}//if (denominator1!0)
			startangle = (360.0/n1*z1)-sector*z2;
			repaint();
		}    //drawPizzaAsCircle

		/**
		 * 
		 *
		 */
		public void noPizzaAsCircle(){
			isVisible = false;
			repaint();
		}

		/**
		 * 
		 */
		@Override
		public void paintComponent(Graphics g){
			Graphics2D screen2D = (Graphics2D)g;
//			screen2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			//Farbe einstellen
			screen2D.setColor(color);
			double nextsektor = 0;
			getShorterSide();
			int links =getWidth()/2 - (shorterside/2 - shorterside/20);
			int oben = getHeight()/2 - (shorterside/2 - shorterside/20);
			if(isVisible){
				//System.out.println("zeichne FractionAsCircle");
				if (denominator != 0){
					if (numerator > denominator){
						//g.drawString("Zaehler > denominator", 10, 15);
						//g.drawString("geht nicht", 10, 30);
						screen2D.setColor(Color.red);
						//System.out.println("Zaehler/denominator: "+numerator+", "+ denominator);
					}else{
						Arc2D.Float arc_numerator = new Arc2D.Float(
							links,				//x
							oben,//shorterside/20,			//y
							shorterside - 2*shorterside/20,	//Width
							shorterside - 2*shorterside/20,	//Height
							(float)startangle,				//start
							(float)sector*numerator,		//Angle
							Arc2D.PIE
						);//Arc
						screen2D.fill(arc_numerator);
						//zeichnet die Sektoren der FractionAsCircle in Schwarz
						screen2D.setColor(Color.black);
						for ( int i = 0; i  <= denominator;  i ++){
							Arc2D.Float arc_i = new Arc2D.Float(
								links,
								oben,//shorterside/20,
								shorterside - 2*shorterside/20,
								shorterside - 2*shorterside/20,
								(float)startangle,
								(float)nextsektor,
								Arc2D.PIE
							);//Arc
							screen2D.draw(arc_i);
							nextsektor = nextsektor + sector;
						}//for
					}//else
				}else{
					//g.drawString("denominator = 0 !!", 10, 15);
				}//if-else
			}else{
				//System.out.println("zeichne keine  !! FractionAsCircle");
				//screen2D.setColor(Color.green);
			}
		}//paint

		/**
		 * 
		 */
		private void getShorterSide(){
			if (getHeight() < getWidth()){
				shorterside = getHeight();
			}else{
				shorterside = getWidth();
			}//if
		}//getKleineSeite

}//pizzaklasse
