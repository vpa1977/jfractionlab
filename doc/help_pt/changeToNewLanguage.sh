#!/bin/sh

# um eine "hilfe" in einer neuen sprache vorzubereiten,
# kopiert man die deutsche hilfe,
# aendert in diesem script das laenderspezifische kuerzel
# und fuehrt dieses script aus

OLD="fr"
NEW="pt"

for i in *html
do
	sed s/"$OLD"".html"/"$NEW"".html"/g $i > $i.p
	mv $i.p $i
done

#sed veraendert auch <html> und </html> :-(
for i in *html
do
	sed s/"<_""$NEW"".html>"/"<\/html>"/g $i > $i.p
	mv $i.p $i
done
for i in *html
do
	sed s/"_""$NEW"".html>"/"<html>"/g $i > $i.p
	mv $i.p $i
done


for i in *.html; 
do 
	#"Alles vor dem 1. Muster bleibt erhalten"
	mv "$i" "${i%%_*}""_""$NEW"".html"
done

